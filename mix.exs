defmodule Rollex.Mixfile do
  use Mix.Project

  def project do
    [
      app: :rollex,
      version: "0.10.0",
      elixir: "~> 1.14",
      name: "Rollex",
      source_url: "https://gitlab.com/r2815/rollex.git",
      homepage_url: "https://gitlab.com/r2815/rollex",
      description: description(),
      deps: deps(),
      package: package(),
      aliases: aliases(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        "coveralls.lcov": :test,
        lcov: :test
      ]
    ]
  end

  # Configuration for the OTP application
  #
  # Tpe `mix help compile.app` for more information
  def application do
    []
  end

  defp aliases do
    [
      lcov: ["coveralls.lcov", "cmd genhtml cover/lcov.info -o cover/lcov"]
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type `mix help deps` for more examples and options
  defp deps do
    [
      {:ex_doc, "~> 0.24", only: :dev},
      {:dialyxir, "~> 1.0", only: :dev, runtime: false},
      {:benchee, "~> 1.0", only: :dev},
      {:excoveralls, "~> 0.15", only: :test},
      {:junit_formatter, "~> 3.3", only: :test}
    ]
  end

  defp description do
    """
    Elixir library using a Pratt Parser algorithm to calculate dice rolls.
    """
  end

  defp package do
    [
      contributors: ["Michael Chmielewski", "Aaron Seigo"],
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/r2815/rollex"}
    ]
  end
end
