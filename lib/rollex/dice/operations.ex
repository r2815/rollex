defmodule Rollex.Dice.Operations do
  @moduledoc """
  Support for operations that can be applied to dice rolls including:

  * odd only (`o`)
  * even oly (`e`)
  * keep high (`kh#, k#`)
  * keep low (`kl#`)
  * drop high (`dh#`)
  * drop low (`dl#`)
  * less than (`<#`)
  * greater than (`>#`)
  * equal to (`=#`, `=[#, #, #..]`, `=[#..#]`)
  * reroll lower than (`r<#`)
  * reroll higher than (`r>#`)
  * reroll equal to (`r=#`, `r=[#, #, #..]`, `r=[#..#]`)
  * explode (`!`)
  """

  @type operation_type ::
          :drop_bottom
          | :drop_top
          | :greater_than
          | :less_than
          | :match
          | :match_list
          | nil
          | :reroll_higher
          | :reroll_lower
          | :reroll_match
          | :reroll_match_list
          | :take_bottom
          | :take_even
          | :take_odd
          | :take_top

  @type operation :: operation_type | {operation_type, params :: term}
  @type operable_token :: %{
          required(:operation) => operation,
          required(:sides) => non_neg_integer,
          atom => any
        }

  @doc "Parse operations from a provided string"
  @spec parse(definition :: String.t()) ::
          {operation, num_chars_consumed :: non_neg_integer}
          | {:error, reason :: String.t()}
  def parse("o" <> _rest), do: {:take_odd, 1}
  def parse("e" <> _rest), do: {:take_even, 1}
  def parse("kh" <> rest), do: op_with_number(rest, :take_top, 2)
  def parse("kl" <> rest), do: op_with_number(rest, :take_bottom, 2)
  def parse("k" <> rest), do: op_with_number(rest, :take_top, 1)
  def parse("dh" <> rest), do: op_with_number(rest, :drop_top, 2)
  def parse("dl" <> rest), do: op_with_number(rest, :drop_bottom, 2)
  def parse("d" <> rest), do: op_with_number(rest, :drop_bottom, 1)
  def parse(">" <> rest), do: op_with_number(rest, :greater_than, 1)
  def parse("<" <> rest), do: op_with_number(rest, :less_than, 1)
  def parse("r>" <> rest), do: op_with_number(rest, :reroll_higher, 2)
  def parse("r<" <> rest), do: op_with_number(rest, :reroll_lower, 2)
  def parse("r=[" <> rest), do: op_with_range(rest, :reroll_match_list, 3)
  def parse("r=" <> rest), do: op_with_number(rest, :reroll_match, 2)
  def parse("=[" <> rest), do: op_with_range(rest, :match_list, 2)
  def parse("=" <> rest), do: op_with_number(rest, :match, 1)
  def parse("!" <> _rest), do: {:explode, 1}
  def parse(_rest), do: {nil, 0}

  @doc "Applies an operation returned by `parse/1` to a list of numbers representing dice rolls"
  @spec apply(token :: operable_token, rolls :: [integer]) ::
          {valid :: [integer], rejected :: [integer]}
  def apply(%{operation: nil}, rolls), do: {rolls, []}

  def apply(%{operation: {:take_top, num_keep}}, rolls) do
    rolls
    |> Enum.sort(&(&1 > &2))
    |> Enum.split(num_keep)
  end

  def apply(%{operation: {:take_bottom, num_keep}}, rolls) do
    rolls
    |> Enum.sort(&(&1 < &2))
    |> Enum.split(num_keep)
  end

  def apply(%{operation: {:drop_top, num_keep}}, rolls) do
    num_rolls = Enum.count(rolls)
    split_at = max(0, num_rolls - num_keep)

    rolls
    |> Enum.sort(&(&1 < &2))
    |> Enum.split(split_at)
  end

  def apply(%{operation: {:drop_bottom, num_keep}}, rolls) do
    num_rolls = Enum.count(rolls)
    split_at = max(0, num_rolls - num_keep)

    rolls
    |> Enum.sort(&(&1 > &2))
    |> Enum.split(split_at)
  end

  def apply(%{operation: {:less_than, ceiling}}, rolls),
    do: Enum.split_with(rolls, &(&1 < ceiling))

  def apply(%{operation: {:greater_than, floor}}, rolls),
    do: Enum.split_with(rolls, &(&1 > floor))

  def apply(%{operation: :take_odd}, rolls), do: Enum.split_with(rolls, &(rem(&1, 2) != 0))
  def apply(%{operation: :take_even}, rolls), do: Enum.split_with(rolls, &(rem(&1, 2) == 0))

  def apply(%{operation: {:match_list, values}}, rolls),
    do: Enum.split_with(rolls, &Enum.member?(values, &1))

  def apply(%{operation: {:match, value}}, rolls),
    do: Enum.split_with(rolls, &(value == &1))

  def apply(%{operation: {:reroll_higher, target}} = token, rolls) do
    Enum.reduce(rolls, {[], []}, fn roll, acc ->
      maybe_reroll(roll > target, token, roll, acc)
    end)
  end

  def apply(%{operation: {:reroll_lower, target}} = token, rolls) do
    Enum.reduce(rolls, {[], []}, fn roll, acc ->
      maybe_reroll(roll < target, token, roll, acc)
    end)
  end

  def apply(%{operation: {:reroll_match, target}} = token, rolls) do
    Enum.reduce(rolls, {[], []}, fn roll, acc ->
      maybe_reroll(roll == target, token, roll, acc)
    end)
  end

  def apply(%{operation: {:reroll_match_list, values}} = token, rolls) do
    Enum.reduce(rolls, {[], []}, fn roll, acc ->
      maybe_reroll(Enum.member?(values, roll), token, roll, acc)
    end)
  end

  def apply(%{operation: :explode, sides: sides}, rolls) when sides < 2 do
    {rolls, []}
  end

  def apply(%{operation: :explode, sides: sides}, rolls) do
    {
      Enum.reduce(rolls, rolls, fn roll, acc ->
        maybe_explode(roll, sides, acc)
      end),
      []
    }
  end

  defp op_with_number(rest, op, consumed) do
    case Regex.run(~r/\d+/, rest) do
      [result | _] ->
        value = Rollex.Utilities.simple_integer_parse(result)
        {{op, value}, String.length(result) + consumed}

      _ ->
        {:error, "Malformed dice operation"}
    end
  end

  defp op_with_range(rest, op, consumed) do
    case Regex.run(~r/(\d+)..(\d+)\]/, rest) do
      nil ->
        case Regex.run(~r/\d+(,\d+)*\]/, rest) do
          nil ->
            {:error, "Invalid range"}

          list ->
            op_vals =
              List.first(list)
              |> String.split(",")
              |> Enum.map(fn x -> Rollex.Utilities.simple_integer_parse(x) end)

            {{op, op_vals}, String.length(List.first(list)) + consumed}
        end

      range ->
        op_range =
          Range.new(
            Rollex.Utilities.simple_integer_parse(Enum.at(range, 1)),
            Rollex.Utilities.simple_integer_parse(Enum.at(range, 2))
          )

        {{op, op_range}, String.length(List.first(range)) + consumed}
    end
  end

  defp maybe_reroll(true, token, roll, {valid, rejected}) do
    reroll = :rand.uniform(token.sides)
    {[reroll | valid], [roll | rejected]}
  end

  defp maybe_reroll(false, _token, roll, {valid, rejected}) do
    {[roll | valid], rejected}
  end

  defp maybe_explode(roll, sides, rolls) when roll == sides do
    explosion = :rand.uniform(sides)
    maybe_explode(explosion, sides, [explosion | rolls])
  end

  defp maybe_explode(_roll, _sides, rolls), do: rolls
end
