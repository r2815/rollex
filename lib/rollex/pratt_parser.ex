defmodule Rollex.PrattParser do
  @moduledoc false

  def evaluate_expression(input, precedence \\ 0)

  def evaluate_expression([%Rollex.Tokens.End{}], _) do
    {:error, "Unexpected end of input!"}
  end

  def evaluate_expression([token | tail], precedence) do
    case Rollex.Token.nud(token, tail) do
      {:ok, left, rest} ->
        _do_led(rest, left, precedence, Rollex.Token.lbp(hd(rest)))

      error ->
        error
    end
  end

  defp _do_led([%Rollex.Tokens.End{}], left, _old_precedence, _new_precedence) do
    {:ok, left, [%Rollex.Tokens.End{}]}
  end

  defp _do_led([token | tail], left, old_precedence, new_precedence)
       when old_precedence < new_precedence do
    case Rollex.Token.led(token, left, tail) do
      {:ok, new_left, new_rest} ->
        _do_led(new_rest, new_left, old_precedence, Rollex.Token.lbp(hd(new_rest)))

      error ->
        error
    end
  end

  defp _do_led(input, left, _old_precedence, _new_precedence), do: {:ok, left, input}
end
