defmodule Rollex.Tokens.Addition do
  @moduledoc false
  @regex ~r/\G\+/
  defstruct regex: @regex

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.Addition do
    @left_binding_precedence 10

    def create(_token, _roll_expr, _matches) do
      {%Rollex.Tokens.Addition{}, 1}
    end

    def nud(_token, _rest) do
      {:error, "Unexpected '+' sign in input"}
    end

    def led(token, left, rest) do
      Rollex.PrattParser.evaluate_expression(rest, Rollex.Token.lbp(token))
      |> perform_led(left)
    end

    def lbp(_token) do
      @left_binding_precedence
    end

    defp perform_led({:ok, right, new_rest}, left) do
      added = Rollex.Utilities.merge(left, right, fn _k, l, r -> l + r end)
      {:ok, added, new_rest}
    end

    defp perform_led(bad_result, _left), do: bad_result
  end
end
