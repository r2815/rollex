defmodule Rollex.Tokens.Subtraction do
  import Rollex.Utilities, only: [is_arithmetic_total?: 1]
  @moduledoc false

  @regex ~r/\G\-/
  defstruct regex: @regex

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.Subtraction do
    @left_binding_precedence 10
    @nud_precedence 100

    def create(_token, _roll_expr, _matches) do
      {%Rollex.Tokens.Subtraction{}, 1}
    end

    def nud(_token, rest) do
      case Rollex.PrattParser.evaluate_expression(rest, @nud_precedence) do
        {:ok, left, rest} ->
          negative =
            left
            |> Enum.into(%{}, fn
              {k, v} when is_arithmetic_total?(k) -> {k, -v}
              entry -> entry
            end)

          {:ok, negative, rest}

        result ->
          result
      end
    end

    def led(token, left, rest) do
      Rollex.PrattParser.evaluate_expression(rest, Rollex.Token.lbp(token))
      |> perform_led(left)
    end

    defp perform_led({:ok, right, new_rest}, left) do
      subtracted = Rollex.Utilities.merge(left, right, fn _k, l, r -> l - r end)
      {:ok, subtracted, new_rest}
    end

    defp perform_led(bad_result, _left), do: bad_result

    def lbp(_token) do
      @left_binding_precedence
    end
  end
end
