defmodule Rollex.Tokens.End do
  @moduledoc false

  @regex ~r/\G\z/
  defstruct regex: @regex

  @type t :: %__MODULE__{}

  defimpl Rollex.Token, for: Rollex.Tokens.End do
    @left_binding_precedence -1

    def create(_token, _roll_expr, _matches) do
      {%Rollex.Tokens.End{}, 0}
    end

    def nud(_token, _rest) do
      {:error, "Unexpected end of input"}
    end

    def led(_token, _left, _rest) do
      {:error, "Unexpected end of input"}
    end

    def lbp(_token) do
      @left_binding_precedence
    end
  end
end
