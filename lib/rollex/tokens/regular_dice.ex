defmodule Rollex.Tokens.RegularDice do
  @moduledoc false

  @regex ~r/\G(\d*)[dD](\d+)/

  defstruct regex: @regex,
            is_dice: true,
            quantity: 0,
            sides: 0,
            operation: nil,
            valid_rolls: [],
            rejected_rolls: [],
            arithmetic: 0.0

  @type t :: %__MODULE__{
          regex: Regex.t(),
          is_dice: boolean,
          quantity: non_neg_integer,
          sides: non_neg_integer,
          operation: Rollex.Dice.Operations.operation(),
          valid_rolls: [integer],
          rejected_rolls: [integer],
          arithmetic: number
        }

  defimpl Rollex.Token, for: Rollex.Tokens.RegularDice do
    @left_binding_precedence 0

    def nud(token, rest) do
      {:ok, %{arithmetic: token.arithmetic, successes: Enum.count(token.valid_rolls)}, rest}
    end

    def led(_token, _left, _rest) do
      {:error, "Unexpected dice token"}
    end

    def lbp(_token) do
      @left_binding_precedence
    end

    def create(_token, roll_expr, [
          {token_start, token_length},
          {quantity_start, quantity_length},
          {sides_start, sides_length}
        ]) do
      parsed_quantity =
        roll_expr
        |> String.slice(quantity_start, quantity_length)
        |> Rollex.Utilities.simple_integer_parse()

      parsed_sides =
        roll_expr
        |> String.slice(sides_start, sides_length)
        |> Rollex.Utilities.simple_integer_parse()

      case Rollex.Dice.Operations.parse(
             String.slice(roll_expr, (token_start + token_length)..-1//1)
           ) do
        {:error, _} = error ->
          error

        {operation, op_length} ->
          total_token_length = token_length + op_length

          {
            %Rollex.Tokens.RegularDice{
              quantity: parsed_quantity,
              sides: parsed_sides,
              operation: operation
            },
            total_token_length
          }
      end
    end
  end

  defimpl Rollex.Dice, for: Rollex.Tokens.RegularDice do
    def roll(%Rollex.Tokens.RegularDice{sides: sides} = token) when sides < 1 do
      %{
        token
        | valid_rolls: List.duplicate(0, token.quantity),
          rejected_rolls: [],
          arithmetic: 0
      }
    end

    def roll(%Rollex.Tokens.RegularDice{} = token) do
      rolls = for _ <- 1..token.quantity, do: :rand.uniform(token.sides)
      {valid, rejected} = Rollex.Dice.Operations.apply(token, rolls)

      %{
        token
        | valid_rolls: valid,
          rejected_rolls: rejected,
          arithmetic: Enum.sum(valid)
      }
    end

    def min(token), do: token.quantity
    def max(token), do: token.sides * token.quantity
  end

  defimpl Rollex.Histogram, for: Rollex.Tokens.RegularDice do
    @spec evaluate(token :: Rollex.Tokens.RegularDice.t(), effort_count :: non_neg_integer) ::
            {Rollex.Histogram.histogram(), effort_count :: non_neg_integer}
    def evaluate(token, current_effort) do
      Rollex.Distribution.histogram(token.quantity, token.sides, current_effort)
    end
  end
end
