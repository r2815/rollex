defmodule RollexTest do
  use ExUnit.Case

  alias Rollex.Tokens

  test "compilation" do
    roll = Rollex.compile("3d8+4d6")

    assert match?(
             [%Tokens.RegularDice{}, %Tokens.Addition{}, %Tokens.RegularDice{}, %Tokens.End{}],
             roll.compiled
           )

    assert roll.valid
    assert roll.error == nil
  end

  test "compilation with dice specified" do
    roll = Rollex.compile("3d8+4d6", [:polyhedral])

    assert match?(
             [%Tokens.RegularDice{}, %Tokens.Addition{}, %Tokens.RegularDice{}, %Tokens.End{}],
             roll.compiled
           )

    assert roll.valid
    assert roll.error == nil
  end

  test "executing compiled rolls" do
    roll = Rollex.compile("3d8+4d6")
    {:ok, result} = Rollex.evaluate(roll)
    {:ok, min} = Rollex.min(roll)
    {:ok, max} = Rollex.max(roll)

    assert result.totals.arithmetic >= min
    assert result.totals.arithmetic <= max
  end

  test "executing strings" do
    {:ok, result} = Rollex.roll("3d8+4d6")
    roll = Rollex.compile("3d8+4d6")
    {:ok, min} = Rollex.min(roll)
    {:ok, max} = Rollex.max(roll)

    assert result.totals.arithmetic >= min
    assert result.totals.arithmetic <= max
  end

  test "zero-sided dice create 0s" do
    {:ok, result} = Rollex.roll("1d00")
    assert result.totals.arithmetic == 0

    {:ok, result} = Rollex.roll("1d0")
    assert result.totals.arithmetic == 0

    {:ok, result} = Rollex.roll("2d0")
    assert result.totals == %{arithmetic: 0, successes: 2}
  end

  test "arithmetic operators only alter numeric sums" do
    {:ok, result} = Rollex.roll("-1d10")
    assert result.totals.arithmetic < 0
    assert result.totals.successes == 1

    {:ok, result} = Rollex.roll("1d10-12")
    assert result.totals.arithmetic < 0
    assert result.totals.successes == 1
  end

  test "malformed rolls" do
    roll = Rollex.compile("3dd8")
    refute roll.valid
    assert roll.error != nil

    assert match?({:error, _}, Rollex.evaluate(roll))
    assert match?({:error, _}, Rollex.evaluate(Rollex.compile("1)")))
    assert match?({:error, _}, Rollex.evaluate(Rollex.compile("1(")))
    assert match?({:error, _}, Rollex.evaluate(Rollex.compile("1/0")))
    assert match?({:error, _}, Rollex.evaluate("3d8"))
    assert match?({:error, _}, Rollex.evaluate(:randomness))

    precedence_issue = Rollex.compile("+3d8")
    assert match?({:error, _}, Rollex.evaluate(precedence_issue))
  end

  test "malformed rolls due to dice used restrictions" do
    roll = Rollex.compile("3d8", [:fudge])
    refute roll.valid
    assert roll.error != nil

    roll = Rollex.compile("3d8", [:paranoia])
    refute roll.valid
    assert roll.error != nil
  end
end
