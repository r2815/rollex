defmodule TokenHistogramTest do
  use ExUnit.Case

  test "create a histogram" do
    token = %Rollex.Tokens.Histogram{}
    assert Rollex.Token.create(token, "anything", []) == {token, 0}
  end

  test "led always errors" do
    token = %Rollex.Tokens.Histogram{}
    left = %{}
    rest = []

    assert match?({:error, _}, Rollex.Token.led(token, left, rest))
  end
end
