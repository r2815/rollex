defmodule DistributiontTest do
  use ExUnit.Case

  def eleven_d_six,
    do: %{
      33 => 4.432,
      12 => 0.0,
      44 => 4.432,
      23 => 0.154,
      29 => 1.778,
      47 => 2.347,
      61 => 0.001,
      30 => 2.347,
      43 => 5.145,
      39 => 6.916,
      45 => 3.702,
      48 => 1.778,
      57 => 0.025,
      26 => 0.625,
      63 => 0.0,
      46 => 2.995,
      31 => 2.995,
      11 => 0.0,
      37 => 6.715,
      32 => 3.702,
      34 => 5.145,
      25 => 0.409,
      28 => 1.301,
      64 => 0.0,
      51 => 0.625,
      38 => 6.916,
      13 => 0.0,
      59 => 0.005,
      40 => 6.715,
      41 => 6.331,
      20 => 0.025,
      15 => 0.0,
      14 => 0.0,
      60 => 0.002,
      58 => 0.012,
      55 => 0.088,
      17 => 0.002,
      22 => 0.088,
      52 => 0.409,
      21 => 0.048,
      36 => 6.331,
      53 => 0.257,
      50 => 0.919,
      66 => 0.0,
      24 => 0.257,
      35 => 5.793,
      56 => 0.048,
      62 => 0.0,
      49 => 1.301,
      27 => 0.919,
      42 => 5.793,
      19 => 0.012,
      65 => 0.0,
      54 => 0.154,
      18 => 0.005,
      16 => 0.001
    }

  def one_d_eight, do: Rollex.Distribution.histogram(1, 8) |> elem(0)
  def two_d_six, do: Rollex.Distribution.histogram(2, 6) |> elem(0)

  def translated_one_d_eight(),
    do: %{
      3 => 12.5,
      4 => 12.5,
      5 => 12.5,
      6 => 12.5,
      7 => 12.5,
      8 => 12.5,
      9 => 12.5,
      10 => 12.5
    }

  defp zipped_histogram(),
    do: %{
      3 => 0.347,
      4 => 1.042,
      5 => 2.083,
      6 => 3.472,
      7 => 5.208,
      8 => 7.292,
      9 => 9.028,
      10 => 10.417,
      11 => 11.111,
      12 => 11.111,
      13 => 10.417,
      14 => 9.028,
      15 => 7.292,
      16 => 5.208,
      17 => 3.472,
      18 => 2.083,
      19 => 1.042,
      20 => 0.347
    }

  test "passing an invalid roll to get a histogram for fails with an error" do
    precedence_problem = Rollex.compile("+1")
    assert(match?({:error, _}, Rollex.histogram(precedence_problem)))

    assert(match?({:error, _}, Rollex.histogram(%Rollex{valid: false})))
    assert(match?({:error, _}, Rollex.histogram(Rollex.compile("(1"))))
    assert(match?({:error, _}, Rollex.histogram(Rollex.compile(")1"))))
    assert(match?({:error, _}, Rollex.histogram(Rollex.compile("1+"))))

    assert(
      Rollex.histogram(%Rollex{valid: false, error: "Error message"}) == {:error, "Error message"}
    )
  end

  test "histogram of 11d6" do
    expected = eleven_d_six()
    assert(match?({^expected, _}, Rollex.Distribution.histogram(11, 6)))
    assert(match?({:ok, %{histogram: ^expected}}, Rollex.histogram(Rollex.compile("11d6"))))
  end

  test "histogram complexity cap respected" do
    {result, _effort} = Rollex.Distribution.histogram(1000, 6)
    assert(Enum.empty?(result))
  end

  test "histogram complexity rollover is respected" do
    {result, _effort} = Rollex.Distribution.histogram(10, 6)
    refute(Enum.empty?(result))

    {result, _effort} = Rollex.Distribution.histogram(10, 6, 11_900_000)
    assert(Enum.empty?(result))

    {:ok, %{histogram: histogram}} = Rollex.histogram(Rollex.compile("1000d6+10d6"))
    assert(Enum.empty?(histogram))
  end

  test "zip histograms" do
    expected = zipped_histogram()

    assert(
      match?(
        {:ok, %{histogram: ^expected}},
        Rollex.histogram(Rollex.compile("1d8+2d6"))
      )
    )

    assert(
      Rollex.Distribution.zip_histograms(one_d_eight(), two_d_six()) ==
        zipped_histogram()
    )
  end

  test "translated histograms" do
    assert(
      Rollex.Distribution.translate_histogram(one_d_eight(), 2, fn _k, l, r -> l + r end) ==
        translated_one_d_eight()
    )
  end

  test "min" do
    assert(Rollex.min(Rollex.compile("1d8")) == {:ok, 1})
    assert(Rollex.min(Rollex.compile("1d8+2")) == {:ok, 3})
    assert(Rollex.min(Rollex.compile("3d8+4d6")) == {:ok, 7})
    assert(match?({:error, _}, Rollex.min(Rollex.compile("3dd6"))))
    assert(match?({:error, _}, Rollex.min(Rollex.compile("+3"))))
    assert(match?({:error, _}, Rollex.min(Rollex.compile("(1"))))
    assert(match?({:error, _}, Rollex.min(Rollex.compile("1("))))
    assert(match?({:error, _}, Rollex.min(Rollex.compile(")1"))))
    assert(match?({:error, _}, Rollex.min(Rollex.compile("1)"))))
  end

  test "max" do
    assert(Rollex.max(Rollex.compile("1d8")) == {:ok, 8})
    assert(Rollex.max(Rollex.compile("1d8+2")) == {:ok, 10})
    assert(Rollex.max(Rollex.compile("3d8+4d6")) == {:ok, 48})
    assert(match?({:error, _}, Rollex.max(Rollex.compile("3dd6"))))
    assert(match?({:error, _}, Rollex.max(Rollex.compile("+3"))))
  end

  test "range" do
    assert(Rollex.range(Rollex.compile("1d8")) == {:ok, 1, 8})
    assert(Rollex.range(Rollex.compile("1d8+2")) == {:ok, 3, 10})
    assert(Rollex.range(Rollex.compile("3d8+4d6")) == {:ok, 7, 48})
    assert(match?({:error, _}, Rollex.range(Rollex.compile("3dd6"))))
    assert(match?({:error, _}, Rollex.range(Rollex.compile("+3"))))
  end
end
