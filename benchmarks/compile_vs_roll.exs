simple_roll = "1d6"
complex_roll = "1d4+2d6+3d4+4d6+5d10+6d12+7d20+2d100d1"

simple_compiled = Rollex.compile(simple_roll)
complex_compiled = Rollex.compile(complex_roll)

config = [memory_time: 2]

Benchee.run(
  %{
    "Simple Rollex.roll" => fn -> Rollex.roll(simple_roll) end,
    "Simple Rollex.evaluate" => fn -> Rollex.evaluate(simple_compiled) end
  },
  config
)

Benchee.run(
  %{
    "Complex Rollex.roll" => fn -> Rollex.roll(complex_roll) end,
    "Complex Rollex.evaluate" => fn -> Rollex.evaluate(complex_compiled) end
  },
  config
)
